<?php

use Illuminate\Database\Seeder;

class SettingTextTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settingsText = [
            array(
                'setting_text_id' => 1,
                'code' => 'sms_generate_code_content',
                'value' => '',
                'major' => 0
            ),
            array(
                'setting_text_id' => 2,
                'code' => 'sms_generate_codes_content',
                'value' => '',
                'major' => 0
            ),
        ];
        DB::table('settings_text')->insert($settingsText);
    }
}
