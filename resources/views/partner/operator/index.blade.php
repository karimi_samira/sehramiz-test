@extends('pool-admin.layout.main')

@section('title', 'لیست اپراتورها')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">اپراتورها</h1>
        <div class="panel panel-default">
            <div class="panel-heading">لیست اپراتورها</div>
            <div class="panel-body">
                <p class="text-left"><a class="btn btn-success" href="{{action('PoolAdmin\OperatorController@getCreate')}}"><i class="fa fa-plus"></i> اپراتور جدید</a></p>
                @if (Session::has('f-message'))
                    <div class="alert alert-{{Session::get('f-message')['t']}}">
                        {!! Session::get('f-message')['m'] !!}
                    </div>
                @endif

                <table class="table table-bordered table-hover datatable">
                    <thead>
                        <tr>
                            <th>نام کاربری</th>
                            <th>ایمیل</th>
                            <th>نام و نام خانوادگی</th>
                            <th>وضعیت</th>
                            <th>عملیات</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($operators as $operator)
                            <tr>
                                <td class="en">{{$operator->username}}</td>
                                <td class="en">{{$operator->email}}</td>
                                <td>{{"$operator->name $operator->last_name"}}</td>
                                <td><span class="label label-{{checkStatus($operator->status, 'success', 'danger')}}">{{checkStatus($operator->status, 'فعال', 'غیر فعال')}}</span></td>
                                <td>
                                    <a href="{{action('PoolAdmin\OperatorController@getUpdate', $operator->operator_id)}}" class="btn btn-info btn-xs" title="ویرایش"><i class="fa fa-pencil"></i></a>
                                    <a href="{{action('PoolAdmin\OperatorController@getDestroy', $operator->operator_id)}}" class="btn btn-danger btn-xs confirm" title="حذف"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop
