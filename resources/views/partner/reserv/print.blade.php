@extends('general.print.main')

@section('content')
<h1 class="text-center">پول تیکت</h1>
<h3 class="text-center">ورودی‌های تاریخ {{to_j($from->format('Y-m-d'), false)}} تا {{to_j($to->format('Y-m-d'), false)}}</h3>
<hr>
<table class="table table-bordered">
    <tr class="text-center">
        <td>جنسیت: {{Helper::printGender($gender)}}</td>
        <td>تعداد رزروها: {{$reserves->count()}} عدد</td>
    </tr>
</table>
<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>#</th>
            <th>نام مشتری</th>
            <th>نام سانس</th>
            <th>زمان سانس</th>
            <th>تعداد بلیت بزرگسال</th>
            <th>تعداد بلیت خردسال</th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 1; ?>
        @foreach ($reserves as $reserv)
        <tr>
            <td>{{$i++}}</td>
            <td>{{$reserv->user_name}}</td>
            <td>{{$reserv->saens_name}}</td>
            <td>{{TicketHelper::displayTime($reserv)}}</td>
            <td class="en">{{$reserv->count_enter_adult}}</td>
            <td class="en">{{$reserv->count_enter_child}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<br>
@stop
