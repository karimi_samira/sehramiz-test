@extends('general.print.main')

@section('title', 'مشاهده فاکتور')

@section('content')
<h2 class="text-center">فاکتور</h2>
<table class="table clean">
    <tr>
        <td>طرف حساب: {{$pool->name}}</td>
        <td class="text-left">تاریخ فاکتور: <span dir="ltr">{{@to_jalali($ledger->date, 'Y/m/d H:i:s l')}}</span></td>
    </tr>
    <tr>
        <td>شماره فاکتور: {{@tr_num($ledger->factor_id, 'fa')}}</td>
        <td class="text-left">ساعت فاکتور: {{@to_time($ledger->date, false, true)}}</td>
    </tr>
    <tr>
        <td>نام سانس: {{$factor->saens_name}}</td>
        <td class="text-left">جنسیت: {{Helper::printGender($factor->gender)}}</td>
    </tr>
</table>
<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>ردیف</th>
            <th>عنوان</th>
            <th>تعداد</th>
            <th>قیمت</th>
            <th>پس از تخفیف</th>
            <th>جمع کل</th>
        </tr>
    </thead>
    <?php $i = 0; ?>
    <tbody>
        @if ($factor->count_adult != '0')
        <tr>
            <td class="en">{{++$i}}</td>
            <td>بزرگسال</td>
            <td class="en">{{$factor->count_adult}}</td>
            <td><span class="en">{{price($factor->price_market_adult)->sep()}}</span> ریال</td>
            <td><span class="en">{{price($factor->price_buy_adult)->sep()}}</span> ریال</td>
            <td><span class="en">{{price($factor->count_adult * $factor->price_buy_adult)->sep()}}</span> ریال</td>
        </tr>
        @endif
        @if ($factor->count_child != '0')
        <tr>
            <td class="en">{{++$i}}</td>
            <td>خردسال</td>
            <td class="en">{{$factor->count_child}}</td>
            <td><span class="en">{{price($factor->price_market_child)->sep()}}</span> ریال</td>
            <td><span class="en">{{price($factor->price_buy_child)->sep()}}</span> ریال</td>
            <td><span class="en">{{price($factor->count_child * $factor->price_buy_child)->sep()}}</span> ریال</td>
        </tr>
        @endif
        <tr>
            <td class="en">{{++$i}}</td>
            <td colspan="3"></td>
            <td class="active">جمع کل:</td>
            <td class="success"><span class="en">{{price($factor->pay_to_pool)->sep()}}</span> ریال</td>
        </tr>
    </tbody>
</table>
@stop
