<div class="pool-helper">
    <div class="input-group">
        <div class="input-group-btn">
            <a href="{{action('Admin\PartnerController@getView', $partner->partner_id)}}" class="btn btn-sm btn-default"><i class="fa fa-user"></i> {{$partner->name}}</a>
            <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="{{action('Admin\PartnerCreditController@getIndex')}}?partner_id={{$partner->partner_id}}" class="text-right"><i class="fa fa-credit-card"></i> اعتبارات</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="{{action('Admin\PartnerController@getUpdate', $partner->partner_id)}}" class="text-right"><i class="fa fa-pencil"></i> ویرایش همکار</a></li>
            </ul>
        </div>
    </div>
</div>
