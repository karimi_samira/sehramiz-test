@extends('admin.layout.main')

@section('title', 'مشاهده اطلاعات کاربر')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">مشاهده اطلاعات کاربر</h1>
        <div class="panel panel-default">
            <div class="panel-heading">مشاهده اطلاعات کاربر
                <a class="btn btn-default btn-xs pull-left" href="{{action('Admin\UserController@getIndex')}}" title="برگشت"><i class="fa fa-reply"></i></a>
            </div>
            <div class="panel-body">
                @if (Session::has('f-message'))
                    <div class="alert alert-{{Session::get('f-message')['t']}}">
                        {!! Session::get('f-message')['m'] !!}
                    </div>
                @endif
                <a href="{{action('Admin\UserController@getUpdate', $user->user_id)}}" class="btn btn-info pull-left"><i class="fa fa-pencil"></i> ویرایش</a>
                <h1>{{$user->name}}</h1>
                <h3>اطلاعات کلی</h3>
                <table class="table table-bordered table-hover datatable">
                    <tr>
                        <th class="active">آی‌دی:</th>
                        <td class="en ltr">{{$user->user_id}}</td>

                        <th class="active">نام:</th>
                        <td>{{$user->name}}</td>
                    </tr>
                    <tr>
                        <th class="active">جنسیت:</th>
                        <td>{{$user->getGender(false, 'نامشخص')}}</td>

                        <th class="active">وضعیت</th>
                        <td><span class="label label-{{checkStatus($user->status, 'success', 'danger')}}">{{checkStatus($user->status, 'فعال', 'غیر فعال')}}</span></td>
                    </tr>
                        <th class="active">ایمیل:</th>
                        <td class="ltr">
                            <span class="en">{{$user->email}}</span>
                        </td>

                        <th class="active">موبایل:</th>
                        <td class="ltr">
                            <span class="en">{{$user->mobile}}</span>
                        </td>
                    </tr>
                    <tr>
                        <th class="active">کد ملی:</th>
                        <td class="en ltr">{{$user->meli_code}}</td>

                        <th class="active">تاریخ تولد:</th>
                        <td>{{Date::make($user->birth_date)->toj()->fa('Y-m-d')}}</td>
                    </tr>
                    <tr>
                        <th class="active">استان:</th>
                        <td>{{$user->state}}</td>

                        <th class="active">شهر:</th>
                        <td>{{$user->city}}</td>
                    </tr>
                    <tr>
                        <th class="active">زمان ثبت نام:</th>
                        <td class="ltr text-right">{{$user->created_at_jalali}}</td>

                        <th class="active">زمان آخرین ویرایش:</th>
                        <td class="ltr text-right">{{$user->updated_at_jalali}}</td>
                    </tr>
                </table>
                <h3>اطلاعات در سایت‌های همکار</h3>
                @foreach ($userPartners as $user)
                <h4>{{$user->partner_name}}</h4>
                <table class="table table-bordered table-hover datatable">
                    <tr>
                        <th class="active">آی‌دی:</th>
                        <td class="en ltr">{{$user->user_id}}</td>

                        <th class="active">نام:</th>
                        <td>{{$user->name}}</td>
                    </tr>
                    <tr>
                        <th class="active">جنسیت:</th>
                        <td>{{$user->getGender(false, 'نامشخص')}}</td>

                        <th class="active"></th>
                        <td></td>
                    </tr>
                        <th class="active">ایمیل:</th>
                        <td class="ltr">
                            <span class="en">{{$user->email}}</span>
                        </td>

                        <th class="active">موبایل:</th>
                        <td class="ltr">
                            <span class="en">{{$user->mobile}}</span>
                        </td>
                    </tr>
                    <tr>
                        <th class="active">کد ملی:</th>
                        <td class="en ltr">{{$user->meli_code}}</td>

                        <th class="active">تاریخ تولد:</th>
                        <td>{{Date::make($user->birth_date)->toj()->fa('Y-m-d')}}</td>
                    </tr>
                    <tr>
                        <th class="active">استان:</th>
                        <td>{{$user->state}}</td>

                        <th class="active">شهر:</th>
                        <td>{{$user->city}}</td>
                    </tr>
                    <tr>
                        <th class="active">زمان ثبت نام:</th>
                        <td class="ltr text-right">{{$user->created_at_jalali}}</td>

                        <th class="active">زمان آخرین ویرایش:</th>
                        <td class="ltr text-right">{{$user->updated_at_jalali}}</td>
                    </tr>
                </table>
                @endforeach
            </div>
        </div>
    </div>
</div>
@stop
