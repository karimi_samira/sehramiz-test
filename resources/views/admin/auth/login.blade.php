<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sehramiz</title>

    <link href="{{asset('admin-assets/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin-assets/dist/css/metisMenu.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin-assets/dist/css/main.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin-assets/dist/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <h2 class="text-center">ورود ادمین</h2>
                    <div class="panel-heading">
                        <h3 class="panel-title">لطفا وارد شوید</h3>
                    </div>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form role="form" method="post" action="{{action('Auth\AdminAuthController@postLogin')}}">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="نام کاربری" name="username" type="text" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="رمز عبور" name="password" type="password" value="">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">مرا به خاطر بسپار
                                    </label>
                                </div>
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button class="btn btn-lg btn-success btn-block">ورود</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{asset('admin-assets/dist/js/jquery.min.js')}}"></script>
    <script src="{{asset('admin-assets/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('admin-assets/dist/js/parsley.min.js')}}"></script>
    <script src="{{asset('admin-assets/dist/js/underscore-min.js')}}"></script>
    <script src="{{asset('admin-assets/datepicker/calendar.js')}}"></script>
    <script src="{{asset('admin-assets/datepicker/jquery.Bootstrap-PersianDateTimePicker.js')}}"></script>
    <script src="{{asset('admin-assets/dist/js/metisMenu.min.js')}}"></script>
    <script src="{{asset('admin-assets/dist/js/main.min.js')}}"></script>
</body>
</html>
