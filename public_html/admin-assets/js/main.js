window.PoolConfig = {};
PoolConfig.begard = {
    template: '<li class="begardp-item"><span class="begardp-selected-remove"><a href="javascript:void(0)" class="btn btn-warning btn-xs"><i class="fa fa-times"></i></a></span><span class="begardp-selected-image"><img width="50px" src=""></span><span class="begardp-selected-text">{file-text}</span></li>',
    inputIndex: 0
};

window.datatableLanguage = {
    processing: "در حال عملیات...",
    search: "جستجو: ",
    lengthMenu: "نمایش _MENU_ سطر",
    info: "نمایش _START_ تا _END_ از _TOTAL_ سطر",
    infoEmpty: "نمایش 0 تا 0 از 0 سطر",
    infoFiltered: '(فیلتر شده از _MAX_ سطر)',
    emptyTable: "هیچ داده‌ایی برای نمایش موجود نیست",
    zeroRecords: "هیچ سطری پیدا نشد",
    paginate: {
        first: "ابتدا",
        last: "انتها",
        next: "بعدی",
        previous: "قبلی"
    }
};

window.Parsley.addValidator('totalMin', {
    validateNumber: function(value, totalMin) {
        var total = 0;
        $('.ps-total-min').each(function() {
            total += parseInt($(this).val());
        })

        return total >= totalMin;
    },
    messages: {
        fa: 'تعداد کل باید بزرگتر از %s باشد.'
    }
});

String.prototype.toFaDigit = function() {
    return this.replace(/\d+/g, function(digit) {
        var ret = '';
        for (var i = 0, len = digit.length; i < len; i++) {
            ret += String.fromCharCode(digit.charCodeAt(i) + 1728);
        }

        return ret;
    });
};


$(function() {
    if (!_.isUndefined($.inputmask)) {
        $('.p-sep-rtl').inputmask({alias: 'decimal', groupSeparator: ',', autoGroup: true, rightAlign: true});

        $('.p-sep-ltr').inputmask({alias: 'decimal', groupSeparator: ',', autoGroup: true, rightAlign: false});
    }

    //Loads the correct sidebar on window load,
    //collapses the sidebar on window resize.
    // Sets the min-height of #page-wrapper to window size
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    $(window).bind("scroll", function() {
        if (window.scrollY <= 35) {
            $("#notification").removeClass("float");
        } else {
            $("#notification").addClass("float");
        }
    });

    $('#side-menu').metisMenu();

    $('.icheck-status.enable').each(function(){
        var self = $(this),
        label = self.prev(),
        label_text = label.text();

        label.remove();
        self.iCheck({
            radioClass: 'icheck-status-r enable',
            insert: '<span>'+label_text+'</span>'
        });
    });

    $('.icheck-status.disable').each(function(){
        var self = $(this),
        label = self.prev(),
        label_text = label.text();

        label.remove();
        self.iCheck({
            radioClass: 'icheck-status-r disable',
            insert: '<span>'+label_text+'</span>'
        });
    });

    $('.show-error').click(function(e) {
        $(this).next().css('display', 'block');
        e.preventDefault();
    });

    $(document).on('click', '.confirm', function(e) {
        var url = $(this).attr('href');
        var message = $(this).attr('data-message');
        if (_.isUndefined(message)) message = window.MSGS.confirm;
        vex.dialog.confirm({
            message: message,
            className: 'vex-theme-default',
            default: 'cancel',
            callback: function(value) {
                if (value)
                    window.location.href = url;
                else return e.preventDefault();
            }
        });

        return e.preventDefault();
    });

    if ($.fn.inputmask) {
        $('.input-mask').inputmask();
    }

    $('.date-picker').MdPersianDateTimePicker({
        Placement: 'bottom',
        Trigger: 'focus',
        EnableTimePicker: false,
        TargetSelector: '',
        GroupId: '',
        ToDate: true,
        FromDate: false,
    });

    $('.datetime-picker').MdPersianDateTimePicker({
        Placement: 'bottom',
        Trigger: 'focus',
        EnableTimePicker: true,
        TargetSelector: '',
        GroupId: '',
        ToDate: true,
        FromDate: false,
    });

    /**
     * TinyMCE initialize
     */
    if (window.tinymce) {
        tinymce.init({
            selector: ".visual-editor",
            content_css: window.INDEX_URL + '/admin-assets/dist/css/tinymce.min.css',
            height: 300,
            plugins: [
                 "advlist autolink link lists charmap print preview hr anchor pagebreak directionality",
                 "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
                 "save table contextmenu directionality template paste textcolor media image pagebreak"
            ],
           toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | ltr rtl | bullist numlist outdent indent | link | media image | forecolor backcolor | print preview fullscreen code | pagebreak",
           directionality: 'rtl',
           pagebreak_separator: '<!-- PAGEBREAK -->'
        });
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    /**
     * Begard - remove file selected
     */
    $(document).on('click', '.begardp-selected-remove a', function() {
        var oldSelected = $(this).closest('.begardp-item');
        if (oldSelected.hasClass('begardp-submitted')) {
            var name = oldSelected.attr('data-name');
            var value = oldSelected.attr('data-value');
            oldSelected.closest('form').append('<input type="hidden" name="' + name + '" value="' + value + '" />');
        }
        var index = oldSelected.attr('data-index');
        oldSelected.remove();
        $('.i-begardp-selected-' + index).remove();
    });

    /**
     * Begard - manage single selected file
     */
    $(document).on('click', '.begardp-select.begardp-select-single', function() {
        var self = $(this);
        var mimes = self.attr('data-mimes');
        var name = self.attr('data-name');

        if (mimes === 'all') {
            mimes = 'all';
        } else {
            mimes = mimes.split(',');
        }

        begard.modal({
            templates: {
                fileDetails: '<h4>جزئیات فایل انتخاب شده</h4><ul><li class="begard-file-details-image"><img class="begard-file-details-image-src" src=""></li><li>نام: {data-name}</li><li>پسوند: {data-extension}</li><li>سایز: {data-size}</li></ul>',
            },
            autoRefresh: true,
            multiSelect: false,
            remote: window.BASE_URL + '/file',
            selectRules: {
                select: true,
                fileSelect: true,
                fileAcceptedMimes: mimes,
                directorySelect: false
            }
        }, {
            afterSelect: function(path, file, directory, customField) {
                var oldSelected = self.closest('.begardp-select-container').find('.begardp-item');
                if (oldSelected.length > 0) {
                    if (oldSelected.hasClass('begardp-submitted')) {
                        var oldDataName = oldSelected.attr('data-name');
                        var oldDataValue = oldSelected.attr('data-value');
                        oldSelected.closest('form').append('<input type="hidden" name="' + oldDataName + '" value="' + oldDataValue + '" />');
                    }
                    var index = oldSelected.attr('data-index');
                    oldSelected.remove();
                    $('.i-begardp-selected-' + index).remove();
                }

                var template = window.PoolConfig.begard.template;
                template = $(template).find('.begardp-selected-text').text(file.name).closest('li');
                template = $(template).attr('data-index', window.PoolConfig.begard.inputIndex);

                if (typeof file.preview !== 'undefined') {
                    template = $(template).find('.begardp-selected-image img').attr('src', file.preview).closest('li');
                } else {
                    template = $(template).find('.begardp-selected-image').addClass('disabled').closest('li');
                }
                self.closest('.begardp-select-container').find('.begardp-selected').append(template);

                self.closest('form').append('<input class="i-begardp-selected-' + window.PoolConfig.begard.inputIndex + '" type="hidden" name="' + name + '[path]" value="' + path +'" />');
                self.closest('form').append('<input class="i-begardp-selected-' + window.PoolConfig.begard.inputIndex + '" type="hidden" name="' + name + '[filename]" value="' + file.name + '" />');

                if (self.hasClass('begardp-with-alt')) {
                    self.closest('form').append('<input class="i-begardp-selected-' + window.PoolConfig.begard.inputIndex + '" type="hidden" name="' + name + '[new_filename]" value="' + customField.filename + '" />');
                    self.closest('form').append('<input class="i-begardp-selected-' + window.PoolConfig.begard.inputIndex + '" type="hidden" name="' + name + '[alt]" value="' + customField.alt + '" />');
                    self.closest('form').append('<input class="i-begardp-selected-' + window.PoolConfig.begard.inputIndex + '" type="hidden" name="' + name + '[title]" value="' + customField.title + '" />');
                }

                if (typeof file.preview !== 'undefined') {
                    self.closest('form').append('<input class="i-begardp-selected-' + window.PoolConfig.begard.inputIndex + '" type="hidden" name="' + name + '[preview]" value="' + file.preview + '" />');
                }

                if (customField.deleteFromGallery) {
                    self.closest('form').append('<input type="hidden" class="i-begardp-selected-' + window.PoolConfig.begard.inputIndex + '" name="' + name + '[delete_after]" value="' + true + '" />');
                }

                window.PoolConfig.begard.inputIndex++;
            },
            addCustomField: function() {
                if (self.hasClass('begardp-with-alt')) {
                    var fields = '<p><label><input id="begardp-delete-from-gallery" type="checkbox" value="true" />  حذف از گالری بعد از انتخاب</label></p>';
                    fields = fields + '<p><label>نام فایل:</label> <input id="begardp-filename" type="text"/></p>';
                    fields = fields + '<p><label>alt فایل:</label> <input id="begardp-alt" type="text"/></p>';
                    return fields + '<p><label>title فایل:</label> <input id="begardp-title" type="text"/></p>';
                }

                return '<label><input id="begardp-delete-from-gallery" type="checkbox" name="deleteAfter" value="true" /> حذف از گالری بعد از انتخاب</label>';
            },
            processCustomField: function() {
                var fields = {};
                if ($('#begardp-delete-from-gallery:checked').is(':checked')) {
                    fields.deleteFromGallery = true;
                } else {
                    fields.deleteFromGallery = false;
                }

                if (self.hasClass('begardp-with-alt')) {
                    fields.filename = $('#begardp-filename').val();
                    fields.alt = $('#begardp-alt').val();
                    fields.title = $('#begardp-title').val();
                }

                return fields;
            }
        });
    });

    $('[data-text-counter]').bind('focus keyup', function() {
        var target = '#' + $(this).attr('data-text-counter');
        $(target).text('تعداد کاراکترها: ' + $(this).val().length);
    });

    $('[data-text-slug]').on('blur', function() {
        var text = $(this).val();

        text = text.toLowerCase()
            .replace(/[()"'%,!.*{}|\\^\[\]`;/?:@&=+$]/g, '')
            .replace(/-+/g, '-')
            .replace(/\s+/g, '-');

        $(this).val(text);
    });

    $('.none-begard-remove-selected a').click(function() {
        var name = $(this).closest('ul').attr('data-name');
        var value = $(this).closest('ul').attr('data-value');

        $(this).closest('form').append('<input type="hidden" name="' + name + '" value="' + value + '">');
        $(this).closest('ul').remove();
    });

    $(document).on('change', '.states-filter', function() {
        var self = $(this);
        $('.city-filter option').remove();
        $.ajax({
            url: window.BASE_URL + '/city/lists?state_id=' + $(this).val(),
            method: 'GET',
            dataType: "json",
            cache: false

        }).done(function(data) {
            if (!data.status) {
                $.notify({message: window.MSGS.eanu},{delay: 2000, type: "danger"});
                return false;
            }
            $('.city-filter option').remove();
            if (self.hasClass('all-cities'))
                $('.city-filter').append('<option value="">همه</option>');
            $.each(data.cities, function(index, city) {
                $('.city-filter').append('<option value=' + city.city_id + '>' + city.name + '</option>');
            });

        }).fail(function(data) {
            $.notify({message: window.MSGS.eanu},{delay: 2000, type: "danger"});
            return false;

        });
    });
});

function removeOverlay(selector) {
    $(selector + ' > .overlay').fadeOut('fast');
    $(selector + ' > .loading-img').fadeOut('fast', function() {
        $(selector + ' > .overlay').remove();
        $(selector + ' > .loading-img').remove();
    });
}

function addOverlay(selector) {
    $(selector).prepend('<div class="loading-img"></div>');
    $(selector).prepend('<div class="overlay"></div>');
}
