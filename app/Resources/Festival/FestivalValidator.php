<?php

namespace Sehramiz\Resources\Festival;

use Auth;
use Illuminate\Http\Request;
use Sehramiz\Resources\Validator;
use Sehramiz\Resources\ValidatorInterface;

class FestivalValidator extends Validator
{
    public function __construct(Request $request)
    {
        $this->mergeRules = true;

        $this->rules = array(
            'name' => 'required',
            'codename' => 'required|unique:festivals,codename',
            'code_price' => 'required|price',
            'code_count_max' => 'numeric',
            'partners' => 'array|exists:partners,partner_id'
        );

        $this->rulesUpdate = array(
            'festival_id' => 'required|exists:festivals,festival_id',
            'codename' => 'required|unique:festivals,codename,'.$request->get('festival_id').',festival_id'
        );

        call_user_func_array('parent::__construct', func_get_args());
    }
}
