<?php namespace Sehramiz\Resources;

interface ValidatorInterface
{
    /**
     * Make rules and return it
     *
     * @return array
     */
    public function makeRules();
}
