<?php

namespace Sehramiz\Classes;

use C;
use Cache;
use Sehramiz\Models\Setting;
use Sehramiz\Models\SettingText;

class Manage
{
    /**
     * Get upload dir of sections
     */
    public static function uploadDir($index)
    {
        $dirs = array(
            'partners'      => 'files/images/partners/',
            'admins'        => 'files/images/admins/',
        );

        return $dirs[$index];
    }

    /**
     * Get Setting
     *
     * @param string $key
     * @param bool $fromText
     * @return string
     */
    public static function getSetting($key, $fromText = false)
    {
        $cacheCode = C::CACHE_SETTING_PREFIX.$key;
        if (Cache::has($cacheCode)) {
            return Cache::get($cacheCode);
        }

        if ($fromText) {
            $setting = SettingText::where('code', $key)->first(['value', 'major']);

            if ($setting->major == 1) {
                Cache::put($cacheCode, $setting->value, 5040);
            }

            return $setting->value;
        } else {
            $setting = Setting::where('code', $key)->first(['value', 'major']);

            if ($setting->major == 1) {
                Cache::put($cacheCode, $setting->value, 5040);
            }

            return $setting->value;
        }
    }
}
