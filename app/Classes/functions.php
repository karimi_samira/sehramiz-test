<?php

/**
 * Get constant of \Sehramiz\Classes\C class
 */
function c($constant) {
    constant("Sehramiz\Classes\C::$constant");
}

/**
 * Return PriceHelper instance
 */
function price($price) {
    return new \Sehramiz\Classes\PriceHelper($price);
}

/**
 * Convert numbers to farsi unicode
 */
function fa($text) {
    return \Date\Jalali::enToFa($text);
}

/**
 * Convert numbers to english unicode
 */
function en($text) {
    return \Date\Jalali::faToEn($text);
}

/**
 * @return \Date\Jalali
 */
function jalali($date = null) {
    return \Date\Jalali::make($date);
}

/**
 * Check status field
 */
 function checkStatus($var, $trueReturn = 1, $falseReturn = 0) {
     if ($var === 1
         || $var === '1'
         || $var === true
         || $var === 'true'
         || $var === 'on') {
         return $trueReturn;
     }
     else {
         return $falseReturn;
     }
 }

/**
 * Real empty
 * 0 is not empty for this function
 */
function real_empty($var) {
    if (is_float($var) || is_int($var) || $var == '0')  return false;

    return empty($var);
}
